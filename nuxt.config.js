module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Divine Sedem Tettey',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Hi there, I am Divine Tettey, a software developer based in Ghana. I love to build stuff. Follow me as i explore the world of code' },
      { name: 'keywords', content:'Divine Tettey, Tettey Divine, Tettey, Divine, Tettey Divine Sedem, Sedem,divine_codez, coding, programmer, java, programmer from ghana, Ghana Programmer, coder, html in ghana, web developer, backend developer, web designer, php, node js'}

    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '/assets/style.css'},
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.0/css/bulma.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700' },
    ]
  },
  /* Layout Transitions */
  layoutTransition: {
    name: "layout",
    mode: ""
  },
  /* Page Transition */
  pageTransition: {
    name: "default",
    mode: ""
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    [
      'nuxt-fontawesome',{
        imports: [
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          },
          {
            set: '@fortawesome/free-brands-svg-icons',
            icons: ['fab']
          }
        ]
      }
    ],
    [
      '@nuxtjs/google-analytics',{
        'id': 'UA-133417736-1'
      }
    ]
  ]
}

